use std::cmp::{Ordering, PartialEq, PartialOrd};

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Sign {
    Pos,
    Neg,
}

impl Sign {
    pub fn invert(&self) -> Sign {
        match self {
            Sign::Pos => Sign::Neg,
            Sign::Neg => Sign::Pos,
        }
    }
}

impl PartialOrd for Sign {
    fn partial_cmp(&self, other: &Sign) -> Option<Ordering> {
        match (self, other) {
            (Sign::Pos, Sign::Neg) => Some(Ordering::Greater),
            (Sign::Neg, Sign::Pos) => Some(Ordering::Less),
            _ => Some(Ordering::Equal),
        }
    }
}
