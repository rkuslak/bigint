/*
 *   BigInt - A simple signed variable-sized integer in Rust
 *   Copyright (C) 2020 Ron Kuslak
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

mod abs;
mod errors;
mod signs;

#[cfg(test)]
mod tests;

use errors::BigIntStringError;
use signs::Sign;
use std::cmp::{Ordering, PartialEq, PartialOrd};
use std::fmt;

const ZERO_CHAR: u8 = '0' as u8;

fn size_bigint_numerals(numerals: &Vec<u8>) -> usize {
    let mut idx = numerals.len();

    while idx > 1 && numerals[idx - 1] == 0 {
        idx -= 1;
    }

    idx
}

#[derive(Clone, Debug)]
pub struct BigInt {
    numerals: Vec<u8>,
    sign: Sign,
}

impl PartialOrd for BigInt {
    fn partial_cmp(&self, other: &BigInt) -> Option<Ordering> {
        match self.sign.partial_cmp(&other.sign) {
            Some(Ordering::Equal) => (),
            ord => return ord,
        };

        let mut idx = size_bigint_numerals(&self.numerals);
        let other_size = size_bigint_numerals(&other.numerals);

        match idx.cmp(&other_size) {
            Ordering::Equal => (),
            // Depending on sign, adjust for a larger negative being "more"
            // negative than a "smaller" negative
            order => match self.sign {
                Sign::Pos => return Some(order),
                Sign::Neg => return Some(order.reverse()),
            },
        };

        // Both are the same "length"; we can now iterate backwards through both
        // vecs and find the first with a order result that is not negative, and
        // return off it:
        while idx > 0 {
            idx -= 1;
            match self.numerals[idx].cmp(&other.numerals[idx]) {
                Ordering::Equal => (),
                order => match self.sign {
                    Sign::Pos => return Some(order),
                    Sign::Neg => return Some(order.reverse()),
                },
            }
        }
        Some(Ordering::Equal)
    }
}

impl Ord for BigInt {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.sign.partial_cmp(&other.sign).unwrap() {
            Ordering::Equal => (),
            ord => return ord,
        };

        let mut idx = size_bigint_numerals(&self.numerals);
        let other_size = size_bigint_numerals(&other.numerals);

        match idx.cmp(&other_size) {
            Ordering::Equal => (),
            // Depending on sign, adjust for a larger negative being "more"
            // negative than a "smaller" negative
            order => match self.sign {
                Sign::Pos => return order,
                _ => return order.reverse(),
            },
        };

        // Both are the same "length"; we can now iterate backwards through both
        // vecs and find the first with a order result that is not negative, and
        // return off it:
        while idx > 0 {
            idx -= 1;
            match self.numerals[idx].cmp(&other.numerals[idx]) {
                Ordering::Equal => (),
                order => match self.sign {
                    Sign::Pos => return order,
                    Sign::Neg => return order.reverse(),
                },
            }
        }
        Ordering::Equal
    }
}

macro_rules! from_intlike {
    ($t: ty) => {
        impl From<$t> for BigInt {
            fn from(origin: $t) -> Self {
                let mut numerals: Vec<u8> = Vec::new();
                let mut sign = Sign::Pos;
                let mut incoming = origin.clone();

                if incoming < 0 {
                    sign = Sign::Neg;
                    incoming = <$t>::abs(incoming);
                }

                while incoming > 0 {
                    let numeral: u8 = (incoming % 10) as u8;
                    numerals.push(numeral);
                    incoming = incoming / 10;
                }
                Self { numerals, sign }
            }
        }
    };
}

macro_rules! from_unsigned_intlike {
    ($t: ty) => {
        impl From<$t> for BigInt {
            fn from(origin: $t) -> Self {
                let mut numerals: Vec<u8> = Vec::new();
                let mut incoming = origin.clone();

                while incoming > 0 {
                    let numeral: u8 = (incoming % 10) as u8;
                    numerals.push(numeral);
                    incoming = incoming / 10;
                }
                Self {
                    numerals,
                    sign: Sign::Pos,
                }
            }
        }
    };
}

from_intlike!(i8);
from_intlike!(i16);
from_intlike!(i32);
from_intlike!(i64);
from_intlike!(i128);
from_unsigned_intlike!(u8);
from_unsigned_intlike!(u16);
from_unsigned_intlike!(u32);
from_unsigned_intlike!(u64);
from_unsigned_intlike!(u128);

impl fmt::Display for BigInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let idx = size_bigint_numerals(&self.numerals);
        let mut numeral_string = String::with_capacity(idx + 1);

        if let Sign::Neg = self.sign {
            numeral_string.push('-');
        }

        // If we have NOTHING to print, ensure at least a 0 is printed
        if idx < 1 {
            numeral_string.push('0');
        }

        for n in self.numerals.iter().rev() {
            numeral_string.push((ZERO_CHAR + n) as char);
        }

        f.write_str(&numeral_string)
    }
}

impl Eq for BigInt {}

impl PartialEq for BigInt {
    fn eq(&self, other: &Self) -> bool {
        match other.cmp(&self) {
            Ordering::Equal => true,
            _ => false,
        }
    }
}

impl BigInt {
    pub fn new() -> Self {
        Self {
            numerals: Vec::<u8>::new(),
            sign: Sign::Pos,
        }
    }

    pub fn try_from<S>(num_str: S) -> Result<BigInt, BigIntStringError>
    where
        S: AsRef<str>,
    {
        let num_str = num_str.as_ref();
        let mut numerals: Vec<u8> = Vec::new();
        let mut sign = Sign::Pos;
        let mut sign_set = false;

        for ch in num_str.chars().into_iter().rev() {
            match ch {
                // Assuming no OTHER non-whitespace characters exist, a sign on
                // the number is fine
                '+' | '-' => {
                    sign_set = true;
                    if ch == '-' {
                        sign = Sign::Neg;
                    }
                }
                // Allow some quality-of-life separators
                ' ' | '_' | ',' => (),
                '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                    if sign_set {
                        return Err(BigIntStringError);
                    }
                    let ch_var = (ch as u8) - ZERO_CHAR;
                    numerals.push(ch_var);
                }
                _ => return Err(BigIntStringError),
            };
        }

        Ok(Self { numerals, sign })
    }

    // Returns size by index of numerals actually contained in the numeral
    // struct. Since the numerals Vec can grow and then after subtraction shrink
    // to a size where contains "leading" zeros
    pub fn len(&self) -> usize {
        size_bigint_numerals(&self.numerals)
    }

    pub fn add(&mut self, other: &BigInt) {
        match other.sign {
            Sign::Neg => {
                let mut other = other.clone();
                other.invert_sign();
                self.subtraction(&other);
            }
            _ => {
                match self.sign {
                    Sign::Pos => self.add_vec(&other.numerals),
                    _ => {
                        // If we are larger than the amount to add, stay
                        // negative and subtract it from us; otherwise, swap the
                        // total from the other in to us and subtract our prior
                        // total
                        match other.cmp(self) {
                            Ordering::Less => self.subtract_vec(&other.numerals),
                            _ => {
                                let mut other_vec = other.numerals.clone();
                                std::mem::swap(&mut self.numerals, &mut other_vec);
                                self.subtract_vec(&other_vec);
                                self.invert_sign();
                            }
                        }
                    }
                }
            }
        };
    }

    pub fn subtraction(&mut self, other: &BigInt) {
        if let Sign::Neg = other.sign {
            // Subtracting a negative is addition. :D
            let mut other = other.clone();
            other.invert_sign();
            self.add(&other);
            return;
        }

        // Subtracting a positive value
        match self.sign {
            Sign::Neg => {
                // Already negative; total can be additive of the new value and
                // our current:
                self.add_vec(&other.numerals);
            }
            Sign::Pos => {
                // We are positive; if the other value is smaller, we can
                // subtract it from us, otherwise normalize to the provided
                // value, and subtract our value:
                match other.cmp(&self) {
                    Ordering::Greater => {
                        let mut new_numerals = other.numerals.clone();
                        std::mem::swap(&mut self.numerals, &mut new_numerals);
                        self.invert_sign();
                        self.subtract_vec(&new_numerals);
                    }
                    _ => self.subtract_vec(&other.numerals),
                };
            }
        }
    }

    // Subtracts the value of the other numerals vec from our vec. Provided vec
    // MUST be of a small value than ours, otherwise this is undefined behavior.
    fn subtract_vec(&mut self, other: &Vec<u8>) {
        let incoming_size = size_bigint_numerals(&other);
        let mut borrow = false;
        let mut idx = 0usize;

        if incoming_size == 0 {
            // Nothing to do; fail silently
            return;
        }

        while idx < incoming_size {
            let mut self_val = self.numerals[idx];
            let mut other_val = match idx.cmp(&incoming_size) {
                Ordering::Greater => 0,
                _ => other[idx],
            };

            if borrow {
                other_val += 1;
            }

            if other_val > self.numerals[idx] {
                self_val += 10;
                borrow = true;
            } else {
                borrow = false;
            }

            self.numerals[idx] = self_val - other_val;
            idx += 1;
        }
    }

    fn add_vec(&mut self, other: &Vec<u8>) {
        // We don't care if the values IN the vec are 0, just that we HAVE
        // values to manipulate:
        let mut size = self.len();

        let incoming_size = size_bigint_numerals(&other);
        let mut carry = 0u8;

        let mut idx = 0usize;
        while idx < incoming_size {
            if size < idx + 1 {
                self.numerals.push(0);
                size += 1;
            }

            let total = carry + self.numerals[idx] + other[idx];
            carry = total / 10;
            self.numerals[idx] = total % 10;
            idx += 1;
        }

        if carry > 0 {
            if size < idx + 1 {
                self.numerals.push(0);
            }
            self.numerals[idx] += carry;
        }
    }

    pub fn invert_sign(&mut self) {
        self.sign = self.sign.invert();
    }
}
