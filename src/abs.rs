pub trait ToAbsolute {
    fn abs(&self) -> Self;
}

macro_rules! signed_abs {
    ($t:ty) => {
        impl ToAbsolute for $t {
            fn abs(&self) -> Self {
                <$t>::abs(*self)
            }
        }
    };
}

macro_rules! unsigned_abs {
    ($t:ty) => {
        impl ToAbsolute for $t {
            fn abs(&self) -> Self {
                *self
            }
        }
    };
}

signed_abs!(i8);
signed_abs!(i16);
signed_abs!(i32);
signed_abs!(i64);
signed_abs!(i128);
unsigned_abs!(u8);
unsigned_abs!(u16);
unsigned_abs!(u32);
unsigned_abs!(u64);
unsigned_abs!(u128);