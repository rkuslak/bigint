mod tests {
    use crate::BigInt;

    #[test]
    fn test_empty_contructor() {
        let test = BigInt::new();

        assert_eq!(test, 0.into());
    }

    #[test]
    fn test_from_str() {
        let test_strs: Vec<&'_ str> = vec![
            &"-10",
            &"10",
            &"999999999999999999999999999999999999999999999",
        ];

        for s in test_strs.iter() {
            let test: BigInt = BigInt::try_from(s).unwrap();
            assert_eq!(test.to_string(), *s);
        }
    }

    #[test]
    fn test_sorted() {
        let mut test_vec: Vec<BigInt> = vec![
            10u16.into(),
            200u32.into(),
            1u64.into(),
            (-10 as i32).into(),
            123504870u64.into(),
            (-123504870 as i64).into(),
        ];
        let expected_vec = vec![
            (-123504870 as i64).into(),
            (-10 as i32).into(),
            1u64.into(),
            10u16.into(),
            200u32.into(),
            123504870u64.into(),
        ];
        test_vec.sort();

        assert_eq!(test_vec.len(), expected_vec.len());

        let mut idx = 0;
        while idx < test_vec.len() {
            assert_eq!(test_vec[idx], expected_vec[idx]);
            idx += 1;
        }
    }

    #[test]
    fn test_negative_addition() {
        let test_cases: Vec<(BigInt, BigInt)> = vec![
            ((-10 as i16).into(), (-10 as i16).into()),
            ((-189 as i16).into(), (-199 as i16).into()),
            (200u32.into(), 1u8.into()),
        ];

        let mut total_test = BigInt::new();
        for test_case in test_cases.iter() {
            total_test.add(&test_case.0);
            assert_eq!(test_case.1, total_test);
        }
    }

    #[test]
    fn test_negative_subtraction() {
        let test_cases: Vec<(BigInt, BigInt)> = vec![
            ((-10 as i16).into(), (10 as i16).into()),
            ((-189 as i16).into(), (199 as i16).into()),
            (200u32.into(), (-1 as i16).into()),
        ];

        let mut total_test = BigInt::new();
        for test_case in test_cases.iter() {
            total_test.subtraction(&test_case.0);
            assert_eq!(test_case.1, total_test);
        }
    }

    #[test]
    fn test_postitive_addition() {
        let test_cases: Vec<(BigInt, BigInt)> = vec![
            (10u16.into(), 10i16.into()),
            (189u128.into(), 199i16.into()),
            (200u32.into(), 399i16.into()),
        ];

        let mut total_test = BigInt::new();
        for test_case in test_cases.iter() {
            total_test.add(&test_case.0);
            assert_eq!(test_case.1, total_test);
        }
    }

    #[test]
    fn test_int_max_addition() {
        // TODO: Put the actual values in here to compare
        let test = std::u64::MAX.into();
        let mut total_test = BigInt::new();
        let max_int = total_test.clone();

        let mut idx = 0;
        while idx < 40 {
            total_test.add(&test);
            idx += 1;
        }

        use std::cmp::Ordering;

        assert_ne!(total_test, max_int);
        assert_eq!(Ordering::Less, max_int.cmp(&total_test));
    }

    #[test]
    fn test_int_min() {
        let test = (std::i64::MIN + 1).into();
        let mut total_test = BigInt::new();
        let max_int = total_test.clone();

        let mut idx = 0;
        while idx < 40 {
            total_test.add(&test);
            idx += 1;
        }

        use std::cmp::Ordering;

        assert_ne!(total_test, max_int);
        assert_eq!(Ordering::Greater, max_int.cmp(&total_test));
    }

    #[test]
    fn test_int_max_subtraction() {
        let test = std::u64::MAX.into();

        let mut total_test = BigInt::new();
        let mut idx = 0;
        while idx < 40 {
            total_test.subtraction(&test);
            idx += 1;
        }
    }
}
